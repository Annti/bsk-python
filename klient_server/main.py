__author__ = 'annti'

from client import *
from server import *
import time


def main():
    server = Server(max_th=4)

    server.register_client("client1", "alamakota")
    server.register_client("client2", "zosiamapsa")
    server.register_client("client3", "coscoscos")
    client1 = Client(server, "client1", "alamakota")
    client2 = Client(server, "client", "sadasdasdas")
    client3 = Client(server, "client3", "alamakota")
    client4 = Client(server, "client", "password2")
    client5 = Client(server, "client2", "zosiamapsa")

    server.start()

    client1.start()
    client2.start()
    client3.start()
    client4.start()
    client5.start()

    time.sleep(6)
    server.kill()


if __name__ == "__main__":
    main()
