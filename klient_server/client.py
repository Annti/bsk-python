__author__ = 'annti'
import random
import hashlib
from threading import Thread
from Queue import Queue


class Client(Thread):
    def __init__(self, server, name, password):
        Thread.__init__(self)
        self.server_to_client = Queue()
        self.client_to_server = Queue()
        self.server_to_client = Queue()
        self.client_to_server = Queue()
        self.server = server
        self.name = name
        self.password = password

    def run(self):
        self.server.clients.put(self)
        print self.read_from_server()
        print self.read_from_server()
        self.write_to_server(self.name)
        print self.read_from_server()
        self.issue_of_challenge(self.password)
        print self.read_from_server()

    def login(self, server, name, password):
        server.clients.put(self)
        print self.read_from_server()
        print self.read_from_server()
        self.write_to_server(name)
        print self.read_from_server()
        self.issue_of_challenge(server, password)
        print self.read_from_server()

    def write_to_server(self, item):
        self.client_to_server.put(item)

    def read_from_server(self):
        while self.server_to_client.empty():
            continue
        return self.server_to_client.get()

    def hashfunction(self, client_challenge, server_challenge, string):
        return hashlib.md5(str(client_challenge)
                           + str(server_challenge) + string).hexdigest()

    def issue_of_challenge(self, password):
        client_challenge = random.random()
        self.write_to_server(client_challenge)
        server_challenge = self.read_from_server()
        self.write_to_server(self.hashfunction(client_challenge, server_challenge, password))
        return self.check_challenge(client_challenge, server_challenge, password)

    def check_challenge(self, client_challenge, server_challenge, password):

        if not self.read_from_server():
            return False
        response_from_server = self.read_from_server()

        if self.hashfunction(server_challenge, client_challenge, password) != response_from_server:
            self.write_to_server(False)
            return False
        self.write_to_server(True)
        return True
