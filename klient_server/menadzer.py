__author__ = 'annti'
from threading import Thread
import hashlib
import random


class Menager(Thread):
    def __init__(self, server):
        self.client = None
        self.login_info = server.login_info
        self.file = server.file

    def run(self, client):
        self.client = client
        print "client said: {0}".format(self.client)
        self.write_to_client("Hello", self.client)
        self.write_to_client("Login:", self.client)
        login = self.read_from_client(self.client).strip()
        self.write_to_client("Password:", self.client)
        if self.challenge(self.client, login):
            self.write_to_client(login + ": Login successful!\n", self.client)
        else:
            self.write_to_client(login + ": Login failed!\n", self.client)

        self.client = None

    def search_password(self, login):
        with open(self.file, "r") as not_passwords:
            lines = not_passwords.readlines()
            for line in lines:
                if line.strip().startswith(login + " "):
                    return line.strip().split(login + " ")[1]

        return None

    def write_to_client(self, item, client):
        client.server_to_client.put(item)

    def read_from_client(self, client):
        while client.client_to_server.empty():
            continue
        return client.client_to_server.get()

    def challenge(self, client, login):
        server_challenge = random.random()
        self.write_to_client(server_challenge, client)
        client_challenge = self.read_from_client(client)
        return self.check_challenge(client, login, client_challenge, server_challenge)

    def is_free(self):
        if self.client is not None:
            return False
        else:
            return True

    def hash_f(self, client_challenge, server_challenge, string):
        return hashlib.md5(str(client_challenge) + str(server_challenge) + string).hexdigest()

    def check_challenge(self, client, login, client_challenge, server_challenge):
        password = self.search_password(login)
        if password == None:
            self.write_to_client(False, client)
            return False
        print "Proper login!!!"
        response_from_client = self.read_from_client(client)
        if self.hash_f(client_challenge, server_challenge, password) != response_from_client:
            self.write_to_client(False, client)
            return False
        print "Proper password!!!"
        self.write_to_client(True, client)
        self.write_to_client(self.hash_f(server_challenge, client_challenge, password), client)
        return self.read_from_client(client)
