__author__ = 'annit'
from threading import Thread
from Queue import Queue
import hashlib
from menadzer import *


class Server(Thread):
    def __init__(self, max_th):
        Thread.__init__(self)
        self.server_to_client = Queue()
        self.clients = Queue()
        self.login_info = dict()
        self.file = "passwords.txt"
        self.max_th = max_th

        self.threads = tuple(
            [Menager(self)
             for _ in xrange(self.max_th)])

        self.is_kill = False

    def run(self):
        while not self.is_kill:
            if not self.clients.empty():
                client_now = self.clients.get()
                th = None
                while th == None:
                    if self.is_kill:
                        break
                    for th in self.threads:
                        if th.is_free():
                            th = th
                if th != None:
                    th.run(client_now)
        print "Server died !!!!!"

    def register_client(self, login, password):
        self.login_info[login] = password
        with open(self.file, "a") as my_file:
            my_file.write(login + " " + password + "\n")

    def hashfunction(self, client_challenge, server_challenge, string):
        return hashlib.md5(str(client_challenge)
                           + str(server_challenge) + string).hexdigest()

    def kill(self):
        self.is_kill = True
