
from OpenSSL import crypto, SSL
from socket import gethostname
from pprint import pprint
from time import gmtime, mktime
from os.path import exists, join

__author__ = 'Aneta'


CERT_FILE = 'Zosia.crt'



def create_self_signed_cert(cert_dir='/home/annti/certyfikatssl'):
    C_F = join(cert_dir, CERT_FILE)

    #podpisujacy
    cert_file = 'Cert_podpisujacy/Aneta.crt'
    cert3 = crypto.load_certificate(crypto.FILETYPE_PEM, open(cert_file).read())
    #podpisywany
    cert_file2 = 'Cert_podpisywany/Kasia.crt'
    cert2 = crypto.load_certificate(crypto.FILETYPE_PEM, open(cert_file2).read())

    if exists(C_F) :

        cert = crypto.X509()
        cert.get_subject().C = cert2.get_subject().C  # dwuliterowy skrot duzymi literami
        cert.get_subject().ST = cert2.get_subject().ST  # np malopolskie
        cert.get_subject().L = cert2.get_subject().L
        cert.get_subject().O = cert2.get_subject().O
        cert.get_subject().OU = cert2.get_subject().OU  # dzial firmy odpowiedzialny za wystawienie certyfikatu
        cert.get_subject().CN = cert2.get_subject().CN


        cert.set_serial_number(cert2.get_serial_number())

        cert.gmtime_adj_notBefore(0)

        cert.gmtime_adj_notAfter(1209600)

        cert.set_issuer(cert3.get_subject())

        cert.set_pubkey(cert2.get_pubkey())
        #close(cert_file2)

        open(C_F, "wt").write(crypto.dump_certificate(crypto.FILETYPE_PEM, cert))

        #close(cert_file)

create_self_signed_cert()

