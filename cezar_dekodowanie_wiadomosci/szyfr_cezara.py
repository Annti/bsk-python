# -*- coding: utf-8 -*-
import codecs, re

file = codecs.open('cipher1.txt', "r", "UTF8")

alfabet = re.split("\s", 'A Ą B C Ć D E Ę F G H I J K L Ł M N Ń O Ó P R S Ś T U W Y Z Ź Ż'.decode("UTF8"))

slownik = set([slowo.upper() for slowo in re.split(",?\s*", codecs.open('odm-utf8.txt', "r", "UTF8").read().strip())])


def dekoduj_slowo(to_slowo, i):
    #konstruktor unicode
    wybrane_slowo = u''
    for sign in to_slowo:
        wybrane_slowo += alfabet[(alfabet.index(sign)+i) % 32]
    return wybrane_slowo

for i in range(32):
    file = codecs.open('cipher1.txt', "r", "UTF8")
    sentencja = []
    zle = []
    procent = 0
    ilosc = 0

    for slowko in re.split("\s", file.read().strip()):
        to_slowo1 = dekoduj_slowo(slowko, i)
        sentencja.append(to_slowo1)
        #jezeli wybiore inna liczbe niz 6 wynik bedzie dokladniejszy
        if len(to_slowo1) > 6:
            ilosc += 1
            if to_slowo1 in slownik:
                procent += 1
            else:
                zle.append(to_slowo1)

    if procent*1./ilosc> 0:

        print str(i)+", "+str(round(procent*1./ilosc, 6))+": "+" ".join(sentencja)
        print " ".join(zle)

    else:
        print str(i)+", "+str(round(procent*1./ilosc, 6))
