
from OpenSSL import crypto, SSL
from socket import gethostname
from pprint import pprint
from time import gmtime, mktime
from os.path import exists, join

__author__ = 'Aneta'

# CN jest, aby dopasowac nazwe hosta naglowka HTTP
CN = raw_input("Input the hostname of the website the certificate is for: ")
CERT_FILE = "%s.crt" % CN
KEY_FILE = "%s.key" % CN



def create_self_signed_cert(cert_dir='ssl_certificate'):
    C_F = join(cert_dir, CERT_FILE)
    K_F = join(cert_dir, KEY_FILE)

	#sprawdzam czy ktorys z kluczy nie istnieje 
	#jezeli nie istnieje to tworze pare
    if not exists(C_F) or not exists(K_F):
        k = crypto.PKey()
        k.generate_key(crypto.TYPE_RSA, 1024)
	    # ten standard certyfikatu potrzebuje wszystkich informacji ponizej
        cert = crypto.X509()
        cert.get_subject().C = raw_input("Country: ") # dwuliterowy skrot
        cert.get_subject().ST = raw_input("State: ")
        cert.get_subject().L = raw_input("City: ")
        cert.get_subject().O = raw_input("Organization: ")
        cert.get_subject().OU = raw_input("Organizational Unit: ")
        cert.get_subject().CN = CN
	#wedlug dokumentacji nie mozna zmienic pliku (/etc/ssl/openssl.cnf)
	#konfiguracyjngo SSL wiec nalezy naklonic do tego uzytkownika
	#numer seryjny certyfikatu
	#wazne!!! powinien byc zwiekszany zawsze kiedy odnawiamy 
	#swiadectwo
        cert.set_serial_number(12)
	#certyfkiat jest wazny dopiero od teraz nie wczesniej
        cert.gmtime_adj_notBefore(0)
	#tu ustawiam czas waznosci certyfikatu tu jakies 2 tygodnie mniejwiecej
        cert.gmtime_adj_notAfter(1209600)
	#stwierdza ze to wystawca jest emiterem (issuer)
        cert.set_issuer(cert.get_subject())
        cert.set_pubkey(k)
	#160-bitowy skrot z wiadomosci o maksymalnym rozmiarze 2^64 bitow i jest oparty na podobnych zasadach co MD5.
        cert.sign(k, 'sha1')


        open(C_F, "wt").write(
        crypto.dump_certificate(crypto.FILETYPE_PEM, cert))

        open(K_F, "wt").write(
        crypto.dump_privatekey(crypto.FILETYPE_PEM, k))

create_self_signed_cert()
